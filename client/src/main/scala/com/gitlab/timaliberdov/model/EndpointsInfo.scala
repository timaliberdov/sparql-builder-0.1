package com.gitlab.timaliberdov.model

import io.circe.Codec
import io.circe.generic.semiauto._

final case class EndpointsInfo(interfaces: Vector[InterfaceInfo])
object EndpointsInfo {
  implicit val eiCodec: Codec[EndpointsInfo] = deriveCodec
}

final case class InterfaceInfo(`type`: String, uri: String, graph: Option[String])
object InterfaceInfo {
  implicit val iiCodec: Codec[InterfaceInfo] = deriveCodec
}
