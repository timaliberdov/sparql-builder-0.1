package com.gitlab.timaliberdov.model

import io.circe.Codec
import io.circe.generic.semiauto._

final case class OntologyInfo(topLevelClasses: Vector[String],
                              subClassSuperClasses: Vector[SubClassSuperClasses],
                              propRanges: Vector[PropRange])
object OntologyInfo {
  implicit val oiCodec: Codec[OntologyInfo] = deriveCodec
}

final case class SubClassSuperClasses(`class`: String,
                                      parents: Vector[String])
object SubClassSuperClasses {
  implicit val scscCodec: Codec[SubClassSuperClasses] = deriveCodec
}

final case class PropRange(property: String, range: String)
object PropRange {
  implicit val cprCodec: Codec[PropRange] = deriveCodec
}
