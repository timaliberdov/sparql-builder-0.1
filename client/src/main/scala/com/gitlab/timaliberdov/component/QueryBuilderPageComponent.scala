package com.gitlab.timaliberdov.component

import com.gitlab.timaliberdov.model._
import com.gitlab.timaliberdov.page.Page
import com.gitlab.timaliberdov.service.AjaxClient
import io.circe.parser.parse
import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.{BackendScope, Unmounted}
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.scalajs.dom
import org.scalajs.dom.ext._
import org.scalajs.dom.html.{Element, Input, Select}
import org.scalajs.dom.raw.HTMLElement

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js

object QueryBuilderPageComponent {

  final case class Props(ctl: RouterCtl[Page])
  final case class State(
                          query: String = "",
                          propsToFindInputs: Int = 1,
                          knownPropsInputs: Int = 1,
                          results: Vector[QueryResponse] = Vector.empty,
                          ontologyInfo: Option[OntologyInfo] = None)

  final class Backend(bs: BackendScope[Props, State]) {

    private def setVisible(selector: String, visible: Boolean = true): Unit =
      dom.document.querySelector(selector) match {
        case element: HTMLElement =>
          element.style.display = if (visible) "block" else "none"
        case _ =>
      }

    private def updateQuery(state: State)(e: ReactEventFromInput): Callback = e.preventDefaultCB >> {
      def collectSelectValuesByName(name: String): Vector[String] =
        dom.document.getElementsByName(name).toVector.collect {
          case e: Select => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      def collectInputValuesByName(name: String): Vector[String] =
        dom.document.getElementsByName(name).toVector.collect {
          case e: Input => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      val propsToFind: Vector[String] =
        collectSelectValuesByName("props-to-find-select")
      val knownProps: Vector[PropWithValAndRange] = {
        val props = collectSelectValuesByName("known-props-select")
        val values = collectInputValuesByName("known-props-values-input")
        props.zip(values).map {
          case (prop, value) => PropWithValAndRange(prop, value, state.ontologyInfo.flatMap(_.propRanges.find(_.property == prop).map(_.range)).getOrElse(""))
        }
      }
      Callback.future {
        Future.fromTry(parse(dom.window.localStorage.getItem("endpointsInfo")).flatMap(_.as[EndpointsInfo]).toTry)
            .flatMap(AjaxClient.getQuery(_)(QueryRequest(propsToFind, knownProps))).recover {
          case e => println(e.getStackTrace.mkString(System.lineSeparator()))
            "Error, sorry"
        }.map(value => bs.modState(_.copy(query = value)))
      }
    }

    def updateResults(state: State)(e: ReactEventFromInput): Callback = e.preventDefaultCB >> {
      def collectSelectValuesByName(name: String): Vector[String] =
        dom.document.getElementsByName(name).toVector.collect {
          case e: Select => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      def collectInputValuesByName(name: String): Vector[String] =
        dom.document.getElementsByName(name).toVector.collect {
          case e: Input => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      val propsToFind: Vector[String] =
        collectSelectValuesByName("props-to-find-select")
      val knownProps: Vector[PropWithValAndRange] = {
        val props = collectSelectValuesByName("known-props-select")
        val values = collectInputValuesByName("known-props-values-input")
        props.zip(values).map {
          case (prop, value) => PropWithValAndRange(prop, value, state.ontologyInfo.flatMap(_.propRanges.find(_.property == prop).map(_.range)).getOrElse(""))
        }
      }
      Callback.future {
        Future.fromTry(parse(dom.window.localStorage.getItem("endpointsInfo")).flatMap(_.as[EndpointsInfo]).toTry)
          .flatMap(AjaxClient.getResults(_)(QueryRequest(propsToFind, knownProps)))
          .recover {
            case e => println(e.getStackTrace.mkString(System.lineSeparator()))
              Vector.empty[QueryResponse]
          }.map(results => bs.modState(_.copy(results = results)))
      }
    }

    def render(props: Props, state: State): VdomTagOf[Element] =
      <.section(
        <.div(
          ^.cls := "container query-builder-page",
          <.div(
            <.h2("SPARQL Query Builder"),
            <.h5(
              ^.cls := "mb-3",
              "Инструмент для конструирования и выполнения запросов по заданной онтологии"
            )
          ),
          <.div(
            ^.cls := "row",
            <.div(
              ^.cls := "column",
              <.table(
                <.thead(
                  <.tr(<.th("Параметры, значения которых нужно найти"))
                ),
                <.tbody(
                  (1 to state.propsToFindInputs).map { _ =>
                    <.tr(
                      <.td(
                        <.div(
                          ^.cls := "input-group",
                          <.select(
                            ^.cls := "custom-select",
                            ^.name := "props-to-find-select",
                            <.option(^.hidden := true),
                            state.ontologyInfo.toVector.flatMap(_.propRanges.map(_.property).distinct.sorted.map(<.option(_))).toTagMod
//                            state.paramOptions.map(p => <.option(p)).toTagMod // todo: label
                          )
                        )
                      ),
                      <.td(
                        <.button(
                          ^.`type` := "button",
                          ^.cls := "btn btn-default btn-circle ml-1",
                          ^.onClick ==> (e => e.preventDefaultCB >> bs.modState(s => s.copy(propsToFindInputs = s.propsToFindInputs + 1))),
                          <.i(^.cls := "fas fa-plus")
                        )
                      )
                    )
                  }.toTagMod
                )
              )
            ),
            <.div(
              ^.cls := "column",
              <.table(
                <.thead(
                  <.tr(
                    <.th("Известные параметры"),
                    <.th("Значения")
                  )
                ),
                <.tbody(
                  (1 to state.knownPropsInputs).map { _ =>
                    <.tr(
                      <.td(
                        ^.style := js.Dictionary("width" -> "50%"),
                        <.div(
                          ^.cls := "input-group",
                          <.select(
                            ^.cls := "custom-select",
                            ^.name := "known-props-select",
                            <.option(^.hidden := true),
                            state.ontologyInfo.toVector.flatMap(_.propRanges.map(_.property).distinct.sorted.map(<.option(_))).toTagMod
//                            state.paramOptions.map(p => <.option(p)).toTagMod // todo: label
                          )
                        )
                      ),
                      <.td(
                        ^.style := js.Dictionary("width" -> "50%"),
                        <.input(^.`type` := "text", ^.cls := "form-control", ^.name := "known-props-values-input")
                      ),
                      <.td(
                        <.button(
                          ^.`type` := "button",
                          ^.cls := "btn btn-default btn-circle ml-1",
                          ^.onClick ==> (e => e.preventDefaultCB >> bs.modState(s => s.copy(knownPropsInputs = s.knownPropsInputs + 1))),
                          <.i(^.cls := "fas fa-plus")
                        )
                      )
                    )
                  }.toTagMod
                )
              )
            )
          ),
          <.div(
            ^.role := "tabpanel",
            <.div(
              ^.role := "tabpanel",
              <.ul(
                ^.cls := "nav nav-tabs",
                ^.role := "tablist",
                <.li(
                  ^.cls := "nav-item",
                  ^.role := "presentation",
                  <.a(
                    ^.cls := "nav-link",
                    ^.href := "#results",
                    ^.aria.controls := "results",
                    ^.role := "tab",
                    VdomAttr("data-toggle") := "tab",
                    "Результаты"
                  )
                ),
                <.li(
                  ^.cls := "nav-item",
                  ^.role := "presentation",
                  <.a(
                    ^.cls := "nav-link",
                    ^.href := "#query",
                    ^.aria.controls := "query",
                    ^.role := "tab",
                    VdomAttr("data-toggle") := "tab",
                    "SPARQL запрос"
                  )
                )
              )
            ),
            <.div(
              ^.cls := "tab-content",
              <.div(
                ^.role := "tabpanel",
                ^.cls := "container tab-pane",
                ^.id := "results",
                <.div(
                  <.button(
                    ^.`type` := "button",
                    ^.cls := "btn btn-default btn-circle btn-right",
                    ^.onClick ==> updateResults(state),
                    <.i(^.cls := "fas fa-sync-alt")
                  )
                ),
                <.div(
                  <.table(
                    ^.cls := "table",
                    <.thead(
                      <.tr(
                        ^.cls := "thead-dark",
                        <.th(
                          ^.scope := "col",
                          "Субъект"
                        ),
                        <.th(
                          ^.scope := "col",
                          "Предикат"
                        ),
                        <.th(
                          ^.scope := "col",
                          "Объект"
                        )
                      )
                    ),
                    <.tbody(
                      state.results.map { res =>
                        <.tr(
                          <.td(
                            <.div(
                              <.p(res.subj)//,
                              //                            res.subj.label.map(<.p(_)).whenDefined
                            )
                          ),
                          <.td(
                            <.div(
                              <.p(res.pred)//,
                              //                            res.pred.label.map(<.p(_)).whenDefined
                            )
                          ),
                          <.td(
                            <.div(
                              <.p(res.obj)//,
                              //                            res.obj.label.map(<.p(_)).whenDefined
                            )
                          )
                        )
                      }.toTagMod
                    )
                  )
                )
              ),
              <.div(
                ^.role := "tabpanel",
                ^.cls := "container tab-pane",
                ^.id := "query",
                <.div(
                  <.button(
                    ^.`type` := "button",
                    ^.cls := "btn btn-default btn-circle btn-right",
                    ^.onClick ==> updateQuery(state),
                    <.i(^.cls := "fas fa-sync-alt")
                  )
                ),
                <.div(
                  <.textarea(
                    ^.cls := "form-control monospaced",
                    ^.rows := 20,
                    ^.disabled := true,
                    ^.value := state.query
                  )
                )
              )
            )
          )
        ),
        <.div(^.cls := "loader")
      )

    def updateInfo(): Callback = {
      val info = dom.window.localStorage.getItem("endpointsInfo")
      println(info)
      println("-" * 80)
      if (info != null) {
        Callback(setVisible(".query-builder-page", visible = false)) >>
          Callback(setVisible(".loader")) >>
        Callback.future {
          Future.fromTry(parse(info).flatMap(_.as[EndpointsInfo]).toTry)
            .flatMap(AjaxClient.getOntologyInfo)
            .map(info => bs.modState(_.copy(ontologyInfo = Some(info))) >>
              Callback(setVisible(".loader", visible = false)) >>
              Callback(setVisible(".query-builder-page")))
        }
      } else Callback(setVisible(".loader", visible = false)) >>
        Callback(setVisible(".query-builder-page"))
    }
  }

  private val component = ScalaComponent
    .builder[Props]("QueryBuilderPage")
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(cdm => cdm.backend.updateInfo())
    .build

  def apply(ctl: RouterCtl[Page]): Unmounted[Props, State, Backend] =
    component(Props(ctl))

}
