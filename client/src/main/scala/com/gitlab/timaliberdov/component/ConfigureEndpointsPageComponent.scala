package com.gitlab.timaliberdov.component

import com.gitlab.timaliberdov.model.{EndpointsInfo, InterfaceInfo}
import com.gitlab.timaliberdov.page.{Page, QueryBuilderPage}
import io.circe.syntax._
import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.scalajs.dom
import org.scalajs.dom.ext._
import org.scalajs.dom.raw.{HTMLInputElement, HTMLOptionElement, HTMLSelectElement}

object ConfigureEndpointsPageComponent {

  final case class Props(ctl: RouterCtl[Page])

  final class Backend(bs: BackendScope[Props, Unit]) {

    def render(props: Props) =
      <.div(
        <.table(
          <.tbody(
            ^.id := "tbl",
            <.tr(
              ^.cls := "infoRow",
              <.td(<.input(^.`type` := "checkbox", ^.cls := "shouldUse")),
              <.td("Server URL: "),
              <.td(<.input(^.`type` := "text", ^.cls := "serverURL", ^.size := 32)),
              <.td("Type: "),
              <.td(<.select(
                ^.cls := "endpointType",
                <.option(^.value := "fuseki", "fuseki"),
                <.option(^.value := "virtuoso", "virtuoso")
              )),
              <.td("Default graph: "),
              <.td(<.input(^.`type` := "text", ^.cls := "defaultGraph"))
            ),
            <.tr(
              ^.cls := "infoRow",
              <.td(<.input(^.`type` := "checkbox", ^.cls := "shouldUse")),
              <.td("Server URL: "),
              <.td(<.input(^.`type` := "text", ^.cls := "serverURL", ^.size := 32)),
              <.td("Type: "),
              <.td(<.select(
                ^.cls := "endpointType",
                <.option(^.value := "fuseki", "fuseki"),
                <.option(^.value := "virtuoso", "virtuoso")
              )),
              <.td("Default graph: "),
              <.td(<.input(^.`type` := "text", ^.cls := "defaultGraph"))
            ),
            <.tr(
              ^.cls := "infoRow",
              <.td(<.input(^.`type` := "checkbox", ^.cls := "shouldUse")),
              <.td("Server URL: "),
              <.td(<.input(^.`type` := "text", ^.cls := "serverURL", ^.size := 32)),
              <.td("Type: "),
              <.td(<.select(
                ^.cls := "endpointType",
                <.option(^.value := "fuseki", "fuseki"),
                <.option(^.value := "virtuoso", "virtuoso")
              )),
              <.td("Default graph: "),
              <.td(<.input(^.`type` := "text", ^.cls := "defaultGraph"))
            ),
            <.tr(
              ^.cls := "infoRow",
              <.td(<.input(^.`type` := "checkbox", ^.cls := "shouldUse")),
              <.td("Server URL: "),
              <.td(<.input(^.`type` := "text", ^.cls := "serverURL", ^.size := 32)),
              <.td("Type: "),
              <.td(<.select(
                ^.cls := "endpointType",
                <.option(^.value := "fuseki", "fuseki"),
                <.option(^.value := "virtuoso", "virtuoso")
              )),
              <.td("Default graph: "),
              <.td(<.input(^.`type` := "text", ^.cls := "defaultGraph"))
            ),
            <.tr(
              ^.cls := "infoRow",
              <.td(<.input(^.`type` := "checkbox", ^.cls := "shouldUse")),
              <.td("Server URL: "),
              <.td(<.input(^.`type` := "text", ^.cls := "serverURL", ^.size := 32)),
              <.td("Type: "),
              <.td(<.select(
                ^.cls := "endpointType",
                <.option(^.value := "fuseki", "fuseki"),
                <.option(^.value := "virtuoso", "virtuoso")
              )),
              <.td("Default graph: "),
              <.td(<.input(^.`type` := "text", ^.cls := "defaultGraph"))
            )
          )
        ),
        <.button(
          ^.onClick --> load(props),
          "Загрузить")
      )

    private def load(props: Props): Callback =
      {
        println("load")
        val interfaceInfos =
          dom.document.getElementsByClassName("infoRow")
            .toVector.filter(_.children.toVector
            .exists(_.children.toVector.collectFirst { case e: HTMLInputElement if e.classList.contains("shouldUse") && e.checked => () }.nonEmpty))
            .flatMap { e =>
              val serverURL = e.children.toVector.flatMap(_.children.toVector).find(_.classList.contains("serverURL")).flatMap {
                case n: HTMLInputElement => Some(n.value).map(_.trim).filter(_.nonEmpty)
                case _ => None
              }
              println(serverURL)
              val endpointType = e.children.toVector.flatMap(_.children.toVector).find(_.classList.contains("endpointType")).flatMap {
                case n: HTMLSelectElement =>
                  n.children.toVector.apply(n.selectedIndex) match {
                    case c: HTMLOptionElement => Some(c.value)
                    case _ => None
                  }
              }
              println(endpointType)
              val graph = e.children.toVector.flatMap(_.children.toVector).find(_.classList.contains("defaultGraph")).flatMap {
                case n: HTMLInputElement => Some(n.value).map(_.trim).filter(_.nonEmpty)
                case _ => None
              }
              println(graph)
              import cats.implicits._
              (endpointType, serverURL).mapN(InterfaceInfo(_, _, graph))
            }
        if (interfaceInfos.nonEmpty) {
          val endpointsInfo = EndpointsInfo(interfaceInfos).asJson.noSpaces
          Callback(dom.window.localStorage.setItem("endpointsInfo", endpointsInfo)) >> props.ctl.set(QueryBuilderPage)
        } else Callback(println("INFO IS EMPTY"))
      }

  }

  private val component = ScalaComponent
    .builder[Props]("ConfigureEndpointsPage")
    .stateless
    .renderBackend[Backend]
    .build

  def apply(ctl: RouterCtl[Page]) = component(Props(ctl))

}
