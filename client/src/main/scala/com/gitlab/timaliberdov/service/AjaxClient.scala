package com.gitlab.timaliberdov.service

import com.gitlab.timaliberdov.model.{EndpointsInfo, OntologyInfo, QueryRequest, QueryResponse}
import io.circe.parser.parse
import io.circe.syntax._
import io.circe.{Decoder, Json}
import org.scalajs.dom.XMLHttpRequest
import org.scalajs.dom.ext.Ajax

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

object AjaxClient {

  private val baseUrl = "/api"

  def getOntologyInfo(endpointsInfo: EndpointsInfo): Future[OntologyInfo] = {
    Ajax.post(s"$baseUrl/info", data = endpointsInfo.asJson.noSpaces).parseEntity[OntologyInfo]
  }

  def getQuery(endpointsInfo: EndpointsInfo)(request: QueryRequest): Future[String] =
    Ajax.post(
      s"$baseUrl/query",
      data = Json.obj("connectionInfo" -> endpointsInfo.asJson, "queryRequest" -> request.asJson).noSpaces
    ).map(_.responseText)

  def getResults(endpointsInfo: EndpointsInfo)(request: QueryRequest): Future[Vector[QueryResponse]] =
    Ajax.post(
      s"$baseUrl/query/results",
      data = Json.obj("connectionInfo" -> endpointsInfo.asJson, "queryRequest" -> request.asJson).noSpaces
    ).parseEntity[Vector[QueryResponse]]

  implicit class RequestFutureExt(f: Future[XMLHttpRequest]) {
    def parseEntity[A: Decoder]: Future[A] =
      f.flatMap(req => Future.fromTry(parse(req.responseText).right.flatMap(_.as[A]).toTry))
  }

}
