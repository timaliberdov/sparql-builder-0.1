package com.gitlab.timaliberdov

import com.gitlab.timaliberdov.component._
import com.gitlab.timaliberdov.page._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router._
import japgolly.scalajs.react.vdom.VdomElement
import org.scalajs.dom

object Client {

  def main(args: Array[String]): Unit = {
    val container = dom.document.getElementById("root")
    val router = Router(BaseUrl.fromWindowOrigin_/, routerConfig)
    router() renderIntoDOM container
  }

  private val routerConfig = RouterConfigDsl[Page].buildConfig { dsl =>
    import dsl._

    (trimSlashes
      | staticRoute("#builder", QueryBuilderPage) ~> renderR(QueryBuilderPageComponent(_))
      | staticRoute("#configure", ConfigureEndpointsPage) ~> renderR(ConfigureEndpointsPageComponent(_))
      | staticRedirect(root) ~> redirectToPage(QueryBuilderPage)(Redirect.Replace)
      )
      .addCondition(page => CallbackTo(page == ConfigureEndpointsPage || dom.window.localStorage.getItem("endpointsInfo") != null))(_ => redirectToPage(ConfigureEndpointsPage)(Redirect.Replace))
      .notFound(redirectToPage(QueryBuilderPage)(Redirect.Replace))
      .verify(QueryBuilderPage, ConfigureEndpointsPage)
      .renderWith(layout)
  }

  private def layout(ctl: RouterCtl[Page], resolution: Resolution[Page]): VdomElement =
    resolution.render()

}

