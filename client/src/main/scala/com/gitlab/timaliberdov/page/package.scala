package com.gitlab.timaliberdov

package object page {

  sealed trait Page
  case object ConfigureEndpointsPage extends Page
  case object QueryBuilderPage extends Page

}
