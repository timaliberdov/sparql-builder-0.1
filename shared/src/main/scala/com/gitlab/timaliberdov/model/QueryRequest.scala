package com.gitlab.timaliberdov.model

final case class QueryRequest(propsToFind: Vector[String], knownProps: Vector[PropWithValAndRange])
object QueryRequest {
  import io.circe.Codec
  import io.circe.generic.semiauto._

  implicit val queryRequestCodec: Codec[QueryRequest] = deriveCodec
}