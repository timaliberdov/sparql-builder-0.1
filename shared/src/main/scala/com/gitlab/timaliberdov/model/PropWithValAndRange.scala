package com.gitlab.timaliberdov.model

final case class PropWithValAndRange(prop: String, value: String, range: String)
object PropWithValAndRange {
  import io.circe.Codec
  import io.circe.generic.semiauto._

  implicit val pwvarCodec: Codec[PropWithValAndRange] = deriveCodec
}
