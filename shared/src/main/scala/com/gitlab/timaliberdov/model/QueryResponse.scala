package com.gitlab.timaliberdov.model

import io.circe.Codec
import io.circe.generic.semiauto._

final case class QueryResponse(subj: String, pred: String, obj: String)
object QueryResponse {
  implicit val qrespCodec: Codec[QueryResponse] = deriveCodec
}
