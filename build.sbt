import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}

val appVersion = "0.2"
val commonSettings = Seq(
  version := appVersion,
  isSnapshot := true,
  scalaVersion := "2.12.8",
  scalacOptions := Seq(
    // format off
    "-encoding", "utf-8",
    "-feature",
    "-language:higherKinds",
    "-Ypartial-unification"
    // format: on
  ),
  parallelExecution in Test := false)

lazy val shared = (crossProject(JSPlatform, JVMPlatform) crossType CrossType.Pure in file("shared"))
  .settings(commonSettings)
  .settings(libraryDependencies ++= Seq(
    "com.lihaoyi" %%% "scalatags" % "0.7.0"))
  .settings(libraryDependencies ++=
    Seq("core", "generic", "parser")
      .map(module => "io.circe" %%% s"circe-$module" % "0.12.0"))
  .jsConfigure(_ enablePlugins ScalaJSWeb)

lazy val sharedJS  = shared.js
lazy val sharedJVM = shared.jvm

val additionalAssets = taskKey[Seq[File]]("copy additional assets to managed resources")

lazy val client = (project in file("client"))
  .settings(commonSettings)
  .settings(
    skip in packageJSDependencies := false,
    scalaJSUseMainModuleInitializer := true,
    dependencyOverrides += "org.webjars.npm" % "js-tokens" % "3.0.2")
  .settings(additionalAssets := {
    val assetsDir = (resourceDirectory in Compile).value / "assets"
    val assets = assetsDir ** "*.*"
    val mappings = assets pair Path.relativeTo(assetsDir)
    val copies = mappings map { case (file, path) => file -> (resourceManaged in Assets).value / path }
    IO.copy(copies)
    copies map (_._2)
  })
  .settings(libraryDependencies ++= Seq(
    "io.suzaku" %%% "diode" % "1.1.4",
    "io.suzaku" %%% "diode-react" % "1.1.4.131",
    "org.scala-js" %%% "scalajs-dom" % "0.9.6",
    "org.querki" %%% "jquery-facade" % "1.2"))
  .settings(libraryDependencies ++=
    Seq("core", "extra")
      .map(module => "com.github.japgolly.scalajs-react" %%% module % "1.4.1"))
  .settings(jsDependencies ++= Seq(
    "org.webjars.npm" % "react" % "16.8.5"
      / "umd/react.development.js"
      minified "umd/react.production.min.js"
      commonJSName "React",
    "org.webjars.npm" % "react-dom" % "16.8.5"
      / "umd/react-dom.development.js"
      minified "umd/react-dom.production.min.js"
      dependsOn "umd/react.development.js"
      commonJSName "ReactDOM",
    "org.webjars.npm" % "react-dom" % "16.8.5"
      / "umd/react-dom-server.browser.development.js"
      minified "umd/react-dom-server.browser.production.min.js"
      dependsOn "umd/react-dom.development.js"
      commonJSName "ReactDOMServer"))
  .dependsOn(sharedJS)
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)

lazy val server = (project in file("server"))
  .settings(commonSettings)
  .settings(libraryDependencies ++=
    Seq("dsl", "circe", "blaze-server", "blaze-client", "scalatags")
      .map(module => "org.http4s" %% s"http4s-$module" % "0.21.0-M6"))
  .settings(libraryDependencies ++= Seq(
    "com.vmunier" %% "scalajs-scripts" % "1.1.2",
    "com.github.pathikrit" %% "better-files" % "3.8.0",
    "ch.qos.logback" % "logback-classic" % "1.2.3"
      exclude("org.slf4j", "slf4j-api")
      exclude("org.slf4j", "slf4j-log4j12")
  ))
  .settings(libraryDependencies ++= Seq(
    "dev.zio" %% "zio" % "1.0.0-RC17",
    "dev.zio" %% "zio-interop-cats" % "2.0.0.0-RC10"
  ))
  .settings(
    scalaJSProjects := Seq(client),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    WebKeys.packagePrefix in Assets := "public/",
    managedClasspath in Runtime += (packageBin in Assets).value)
  .settings(sourceGenerators in Assets += (additionalAssets in client).taskValue)
  .settings(
    imageNames in docker := Seq(
      ImageName(s"timaliberdov/sparql-builder:$appVersion"),
      ImageName("timaliberdov/sparql-builder:latest")
    ),
    dockerfile in docker := {
      val appDir: File = stage.value
      val targetDir = "/app"

      new Dockerfile {
        from("openjdk:8-jre")
        entryPoint(s"$targetDir/bin/${executableScriptName.value}")
        copy(appDir, targetDir)
      }
    }
  )
  .dependsOn(sharedJVM)
  .enablePlugins(SbtWeb, JavaAppPackaging, sbtdocker.DockerPlugin)

onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value
