package com.gitlab.timaliberdov

import _root_.scalatags.Text.all._
import _root_.scalatags.Text.tags.html
import _root_.scalatags.Text.tags2
import cats.data._
import cats.effect._
import cats.implicits._
import com.gitlab.timaliberdov.Server.AppEnv
import com.gitlab.timaliberdov.model.{QueryRequest, QueryResponse}
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe._
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.io._
import org.http4s.headers.Accept
import org.http4s.implicits._
import org.http4s.scalatags._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import zio._
import zio.blocking.Blocking
import zio.console._
import zio.interop.catz._
import zio.interop.catz.implicits._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration

object Server extends CatsApp {
  private def httpApp(blockingEC: ExecutionContext)(implicit runtime: Runtime[AppEnv]) = Router[Task](
    "/" -> Routes.appRoutes(Blocker.liftExecutionContext(blockingEC)),
    "/api" -> Routes.routes
  ).orNotFound

  type AppEnv = OntologyInfoCache with OntologyInfoCache.Env
  override def run(args: List[String]): URIO[ZEnv, Int] =
    for {
      ontologyCache <- Ref.make(Map.empty[String, OntologyInfo])
      classRef <- Ref.make(Map.empty[String, Class])
      subClassRef <- Ref.make(Map.empty[String, Set[Class]])
      propRef <- Ref.make(Map.empty[String, Property])
      cacheWrapper = new OntologyInfoCache.RefWrapper(ontologyCache)
      ontInfoServiceWrapper = new OntologyInfoService.RefWrapper(classRef, subClassRef, propRef)
      env = new Console.Live with Blocking.Live with ontInfoServiceWrapper.Live with cacheWrapper.Live {}
      blockingEC <- env.blocking.blockingExecutor
      rt <- ZIO.runtime[OntologyInfoCache.Env with OntologyInfoCache].provide(env)
      res <- BlazeServerBuilder[Task]
        .bindHttp(9000, "0.0.0.0")
        .withHttpApp(httpApp(blockingEC.asEC)(rt))
        .withIdleTimeout(Duration.Inf)
        .serve
        .compile
        .drain
        .fold(_ => 1, _ => 0)
    } yield res
}

trait OntologyInfoCache {
  val ontologyInfoCache: OntologyInfoCache.Service
}
object OntologyInfoCache {
  def apply(): URIO[OntologyInfoCache, Service] =
    ZIO.access(_.ontologyInfoCache)

  type Env = OntologyInfoService with OntologyInfoService.Env

  trait Service {
    def get(connection: SparqlConnection): ZIO[Env, Throwable, OntologyInfo]
  }

  class RefWrapper(ref: Ref[Map[String, OntologyInfo]]) {
    trait Live extends OntologyInfoCache {
      override val ontologyInfoCache: Service = new Service {
        override def get(connection: SparqlConnection): ZIO[Env, Throwable, OntologyInfo] = for {
          infos <- ref.get
          key = connection.key
          info <- infos
            .get(key)
            .fold(updateRef(key)(connection))(ZIO.succeed)
        } yield info

        private def updateRef(key: String)(connection: SparqlConnection) = for {
          infoService <- OntologyInfoService()
          info <- infoService.get(connection)
          _ <- ref.update(_.updated(key, info))
        } yield info
      }
    }
  }
}

trait OntologyInfoService {
  val ontologyInfoService: OntologyInfoService.Service
}
object OntologyInfoService extends Http4sClientDsl[Task] {
  def apply(): URIO[OntologyInfoService, Service] =
    ZIO.access(_.ontologyInfoService)

  type Env = Console with Blocking

  trait Service {
    def get(connection: SparqlConnection): ZIO[Env, Throwable, OntologyInfo]
  }

  final class RefWrapper(classRef: Ref[Map[String, Class]],
//                         classPropsRef: Ref[Map[String, Set[Property]]],
                         subClassRef: Ref[Map[String, Set[Class]]],
//                         enumRef: Ref[Map[String, Set[String]]],
                         propertyRef: Ref[Map[String, Property]])
                        (implicit runtime: Runtime[Any]) {
    trait Live extends OntologyInfoService {
      override val ontologyInfoService: Service = new Service {
        override def get(connection: SparqlConnection): ZIO[Env, Throwable, OntologyInfo] = for {
          executor <- zio.blocking.blockingExecutor
          consoleEnv <- ZIO.environment[Console]
          _ <- BlazeClientBuilder[Task](executor.asEC, None).resource.use { client =>
            ZIO.foreach_(connection.interfaces)(load(client)(_).provide(consoleEnv))
          }
          classes <- classRef.get
//          classProps <- classPropsRef.get
          subClasses <- subClassRef.get
//          enums <- enumRef.get
          props <- propertyRef.get
        } yield OntologyInfo(classes, /*classProps,*/ subClasses, /*enums,*/ props)

        private def filter(varName: String, clause: String = ""): String =
          s"filter (!regex(str(?$varName),'^nodeID://') && !isBlank(?$varName) $clause)"

        private def buildListMember(varName: String, classVar: String, int: Int = 0, filter: String = ""): String =
          s"{ $varName rdf:rest* ?Rest$int . ?Rest$int rdf:first $classVar $filter . }"

        private val superSubClassQuery =
          s"""prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
             |
             |select distinct ?x ?y where {
             |  ?x rdfs:subClassOf ?y
             |  ${filter("x")}
             |  ${filter("y")}
             |  filter (?x != ?y) .
             |}""".stripMargin

        private val topLevelClassQuery =
          s"""prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
             |prefix owl: <http://www.w3.org/2002/07/owl#>
             |prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
             |
             |select distinct ?Class where {
             |  ?Class rdf:type owl:Class
             |  ${filter("Class")} .
             |  minus {
             |    ?Class rdfs:subClassOf ?Sup
             |    ${filter("Sup")}
             |    filter (?Sup != owl:Thing)
             |    filter (?Class != ?Sup).
             |  }
             |}""".stripMargin

        private val propertyQuery =
          s"""prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
             |prefix owl: <http://www.w3.org/2002/07/owl#>
             |prefix xsd: <http://www.w3.org/2001/XMLSchema#>
             |prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
             |
             |select distinct ?Property ?Range
             |where {
             |  {
             |    [] ?Property [] .
             |    optional {
             |      ?Property rdfs:range ?Range
             |      ${filter("Range", "|| regex(str(?Range),'XML')")}
             |    } .
             |  } union {
             |    ?Property rdfs:domain ?Class
             |    ${filter("Class")} .
             |    ?Property rdfs:range ?Range
             |    ${filter("Range", "|| regex(str(?Range),'XML')")} .
             |  } union {
             |    ?Property rdfs:domain ?x .
             |    ?x owl:unionOf ?y .
             |    ${buildListMember("?y", "?Class")} .
             |    ?Property rdfs:range ?Range
             |    ${filter("Range", "|| regex(str(?Range),'XML')")} .
             |  } union {
             |    ?Property rdfs:domain ?Class
             |    ${filter("Class")} .
             |    ?Property rdfs:range ?x .
             |    ?x owl:unionOf ?y .
             |    ${buildListMember("?y", "?Range", 1, filter("Range", "|| regex(str(?Range),'XML')"))}
             |  } union {
             |    ?Property rdfs:domain ?x .
             |    ?x owl:unionOf ?y .
             |    ${buildListMember("?y", "?Class", 2, filter("Class"))} .
             |    ?Property rdfs:range ?x1 .
             |    ?x1 owl:unionOf ?y1 .
             |    ${buildListMember("?y1", "?Range", 3, filter("Range", "|| regex(str(?Range),'XML')"))}
             |  } union {
             |    ?Class rdfs:subClassOf ?x
             |    ${filter("Class")} .
             |    ?x rdf:type owl:Restriction .
             |    ?x owl:onProperty ?Property .
             |    ?x owl:onClass ?Range
             |    ${filter("Range", "|| regex(str(?Range),'XML')")} .
             |  } union {
             |    ?Class rdfs:subClassOf ?x
             |    ${filter("Class")} .
             |    ?x rdf:type owl:Restriction .
             |    ?x owl:onProperty ?Property .
             |    ?x owl:onClass ?y .
             |    ?y owl:unionOf ?z .
             |    ${buildListMember("?z", "?Range", 4, filter("Range", "|| regex(str(?Range),'XML')"))}
             |  } union {
             |    ?x1 owl:unionOf ?x2 .
             |    ${buildListMember("?x2", "?Class", 5, filter("Class"))} .
             |    ?x1 rdfs:subClassOf ?x .
             |    ?x rdf:type owl:Restriction .
             |    ?x owl:onProperty ?Property .
             |    ?x owl:onClass ?y .
             |    ?y owl:unionOf ?z .
             |    ${buildListMember("?z", "?Range", 6, filter("Range", "|| regex(str(?Range),'XML')"))}
             |  } union {
             |    ?Class rdfs:subClassOf ?x
             |    ${filter("Class")} .
             |    ?x rdf:type owl:Restriction .
             |    ?x owl:onProperty ?Property .
             |    ?x owl:someValuesFrom ?Range
             |    ${filter("Range", "|| regex(str(?Range),'XML')")} .
             |  } union {
             |    ?Class rdfs:subClassOf ?x
             |    ${filter("Class")} .
             |    ?x rdf:type owl:Restriction .
             |    ?x owl:onProperty ?Property .
             |    ?x owl:allValuesFrom ?Range
             |    ${filter("Range", "|| regex(str(?Range),'XML')")} .
             |  }
             |}
             |""".stripMargin


        //        private val enumQuery =
        //          s"""prefix owl: <http://www.w3.org/2002/07/owl#>
        //             |prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        //             |
        //             |select ?Class ?EnumVal
        //             |where {
        //             |  ?Class owl:equivalentClass ?ec
        //             |  ${filter("Class")} .
        //             |	?ec owl:oneOf ?c .
        //             |	?c rdf:rest*/rdf:first ?EnumVal .
        //             |}
        //             |""".stripMargin

        private def addClass(c: Class): UIO[Unit] = for {
          _ <- classRef.update(_.updated(c.name.fullName, c))
          _ <- ZIO.foreach_(c.parentNames) { scn =>
            subClassRef.update { map =>
              val newVal = map.get(scn.fullName) match {
                case None => Set(c)
                case Some(cs) => cs + c
              }
              map.updated(scn.fullName, newVal)
            }
          }
        } yield ()

        private def loadSuperSubClasses(client: Client[Task], endpoint: SparqlEndpoint) = for {
          resp <- sendRequest(client, endpoint)(superSubClassQuery)
          superSubResp <- parseResponse(resp)
          subClasses = getStringResultsColumn(superSubResp, "x")
          superClasses = getStringResultsColumn(superSubResp, "y")
          _ <-
            if (subClasses.size == superClasses.size) ZIO.unit
            else ZIO.fail(new IllegalStateException(s"Got different sub classes (${subClasses.size}) and super classes (${superClasses.size}) size"))
          classes = subClasses.zip(superClasses)
            .collect { case (Some(x), y) => (x, y) }
            .groupBy { case (x, _) => x }
            .map { case (x, ys) => Class(Name(x), ys.flatMap { case (_, y) => y.map(Name) }) }
          _ <- ZIO.foreach_(classes)(addClass)
        } yield ()

        private def loadTopLevelClasses(client: Client[Task], endpoint: SparqlEndpoint) = for {
          resp <- sendRequest(client, endpoint)(topLevelClassQuery)
          topLevelClassesResp <- parseResponse(resp)
          topLevelClasses = getStringResultsColumn(topLevelClassesResp, "Class")
          classes = topLevelClasses.collect {
            case Some(cn) =>
              Class(Name(cn), Vector.empty)
          }
          _ <- ZIO.foreach_(classes)(addClass)
        } yield ()

        private def loadProperties(client: Client[Task], endpoint: SparqlEndpoint) = for {
          resp <- sendRequest(client, endpoint)(propertyQuery)
          parsedResp <- parseResponse(resp)
//          classes = getStringResultsColumn(parsedResp, "Class")
          properties = getStringResultsColumn(parsedResp, "Property")
//          _ <-
//            if (classes.size == properties.size) ZIO.unit
//            else ZIO.fail(new IllegalStateException(s"Got different classes (${classes.size}) and properties (${properties.size}) size"))
          ranges = getOptStringResultsColumn(parsedResp, "Range")
          _ <-
            if (properties.size == ranges.size) ZIO.unit
            else ZIO.fail(new IllegalStateException(s"Got different properties (${properties.size}) and ranges (${ranges.size}) size"))
          _ <- ZIO.foreach_(properties.zip(ranges)) {
            case (Some(p), r) =>
//              classRef.get.flatMap { classMap =>
//                if (classMap.get(c).isEmpty) {
//                  putStrLn(s"!!!!!!!! WARNING: CLASS NOT FOUND: $c for $p with $r")
//                } else {
//
//                }
//              }
              val property = Property(Name(p), Range(r.getOrElse("")))
              propertyRef.update(_.updated(property.name.fullName, property))
            case _ => ZIO.unit
          }
        } yield ()

//        private def loadEnums(client: Client[Task], endpoint: SparqlEndpoint) = for {
//          resp <- sendRequest(client, endpoint)(enumQuery)
//          parsedResp <- parseResponse(resp)
//          classes = getStringResultsColumn(parsedResp, "Class")
//          enumVals = getStringResultsColumn(parsedResp, "EnumVal")
//          _ <-
//            if (classes.size == enumVals.size) ZIO.unit
//            else ZIO.fail(new IllegalStateException(s"Got different classes (${classes.size}) and enums (${enumVals.size}) size"))
//          pairs = classes.zip(enumVals)
//            .collect { case (Some(x), y) => (x, y) }
//            .groupBy { case (x, _) => x }
//            .mapValues(_.flatMap { case (_, y) => y })
//          _ <- ZIO.foreach_(pairs) { case (classKey, enums) =>
//            enumRef.update(_.updatedWith(classKey)(_.fold(enums.toSet)(_ ++ enums).some))
//          }
//        } yield ()

        private def load(client: Client[Task])(endpoint: SparqlEndpoint): RIO[Console, Unit] = for {
          _ <- loadSuperSubClasses(client, endpoint)
          _ <- loadTopLevelClasses(client, endpoint)
          _ <- loadProperties(client, endpoint)
//          _ <- loadEnums(client, endpoint)
        } yield ()
      }
    }
  }

  final case class ParsedJsonResponse(vars: Vector[String], bindings: Vector[Json])

  def sendRequest(endpoint: SparqlEndpoint)(query: String)(implicit runtime: Runtime[Any]): RIO[Blocking, Json] = for {
    executor <- zio.blocking.blockingExecutor
    res <- BlazeClientBuilder[Task](executor.asEC, None).resource.use { client =>
      sendRequest(client, endpoint)(query)
    }
  } yield res

  def sendRequest(client: Client[Task], endpoint: SparqlEndpoint)(query: String): Task[Json] = {
    val request = POST.apply(
      UrlForm(
        Vector(
          ("format" -> "application/sparql-results+json").some,
          ("query" -> query).some,
          endpoint.defaultGraph.map("default-graph-uri" -> _)
        ).flatten: _*
      ),
      Uri.fromString(endpoint.postUrl).right.get,
      Accept(new MediaType("application", "sparql-results+json", true, true, List("json")))
    )
    client.expect[Json](request)
  }

  def parseResponse(json: Json): Task[ParsedJsonResponse] = {
    val headVars: Option[Vector[String]] =
      json
        .hcursor
        .downField("head")
        .downField("vars")
        .focus
        .flatMap(_.asArray)
        .map(_.flatMap(_.asString))

    val resBindings: Option[Vector[Json]] =
      json
        .hcursor
        .downField("results")
        .downField("bindings")
        .focus
        .flatMap(_.asArray)

    ZIO.fromOption((headVars, resBindings).mapN(ParsedJsonResponse))
      .mapError(_ => new IllegalArgumentException("Invalid JSON response from endpoint"))
  }

  def getOptStringResultsColumn(resp: ParsedJsonResponse, column: String): Vector[Option[String]] =
    resp.vars
      .find(_ == column)
      .fold(resp.bindings.map(_ => Option.empty[String])) { _ =>
        resp.bindings.map { b =>
          b.hcursor
            .downField(column)
            .downField("value")
            .focus
            .flatMap(_.asString)
        }
      }

  def getStringResultsColumn(resp: ParsedJsonResponse, column: String): Vector[Option[String]] =
    resp.vars
      .find(_ == column)
      .flatTraverse { _ =>
        resp.bindings.flatMap { b =>
          b.hcursor
            .downField(column)
            .downField("value")
            .focus
            .map(_.asString)
        }
      }
}

final case class OntologyInfo(classes: Map[String, Class],
                              //classProperties: Map[String, Set[Property]],
                              subClasses: Map[String, Set[Class]],
                              //                              enums: Map[String, Set[String]],
                              properties: Map[String, Property]) {
  def toJson: Json = {
    final case class TopLevelClass(c: String)
    object TopLevelClass {
      implicit val tlcEncoder: Encoder[TopLevelClass] = (tlc: TopLevelClass) => Json.fromString(tlc.c)
    }
    final case class SubClassSuperClasses(sub: String, sup: NonEmptyChain[String])
    object SubClassSuperClasses {
      implicit val scscEncoder: Encoder[SubClassSuperClasses] = (scsc: SubClassSuperClasses) => Json.obj(
        "class" -> Json.fromString(scsc.sub),
        "parents" -> Json.arr(scsc.sup.map(Json.fromString).toList: _*)
      )
    }

    val (topLevelClasses, subClassSuperClasses) =
      classes.partition { case (_, cls) => cls.parentNames.isEmpty }.bimap(
        _.keys.map(TopLevelClass(_)),
        _.map { case (classKey, cls) => SubClassSuperClasses(classKey, NonEmptyChain.fromSeq(cls.parentNames.map(_.fullName)).get) }
      )

    final case class PropRange(p: String, r: String)
    object PropRange {
      implicit val cprEncoder: Encoder[PropRange] = (cpr: PropRange) => Json.obj(
        "property" -> Json.fromString(cpr.p),
        "range" -> Json.fromString(cpr.r)
      )
    }
    val propRanges = properties.values.map(prop => PropRange(prop.name.fullName, prop.range.fullName))

//    final case class ClassEnumVal(c: String, v: String)
//    object ClassEnumVal {
//      implicit val cevEncoder: Encoder[ClassEnumVal] = (cev: ClassEnumVal) => Json.obj(
//        "class" -> Json.fromString(cev.c),
//        "value" -> Json.fromString(cev.v)
//      )
//    }
    //    val classEnumValues = enums.flatMap { case (c, vals) =>
    //      vals.map(ClassEnumVal(c, _))
    //    }

    Json.obj(
      "topLevelClasses" -> topLevelClasses.asJson,
      "subClassSuperClasses" -> subClassSuperClasses.asJson,
      "propRanges" -> propRanges.asJson,
      //      "classEnumValues" -> classEnumValues.asJson,
    )
  }
}

object Routes extends Http4sDsl[Task] {

  // todo: move to service
  private def generateSelect(interfaces: Vector[SparqlEndpoint])(qr: QueryRequest): String = {
    def generateService(endpoint: SparqlEndpoint): String =
      s"""service <${endpoint.postUrl}> {
         |  ?s ?p ?o ${if (qr.knownProps.nonEmpty) { ";\n     " + qr.knownProps.map(p => s"""<${p.prop}> ${if (p.range.nonEmpty) s""""${p.value}"^^<${p.range}>""" else Uri.fromString(p.value).fold(_ => '"' + p.value + '"', _ => "<" + p.value + ">") }""").mkString(" ;\n     ") + (if (qr.knownProps.nonEmpty) " ." else "") } else "." }
         |  ${val nonEmpty = qr.propsToFind.nonEmpty; (if (nonEmpty) "values ?p {\n    " else "") + qr.propsToFind.map("<" + _ + ">").mkString("\n    ") + (if (nonEmpty) "\n  }" else "") }
         |}""".stripMargin

    def union(selects: Vector[String]): String =
      selects.mkString("{\n  ", "\n} union {\n  ", "\n}")

    s"""select distinct ?s ?p ?o where {
          |${union(interfaces.map(generateService))}
          |}""".stripMargin
  }

  private implicit def vectorJsonEncoder[A: Encoder]: EntityEncoder[Task, Vector[A]] =
    org.http4s.circe.jsonEncoderOf[Task, Vector[A]]
  private implicit def vectorJsonDecoder[A: Decoder]: EntityDecoder[Task, Vector[A]] =
    org.http4s.circe.jsonOf[Task, Vector[A]]

  private implicit val qRespEncoder: EntityEncoder[Task, QueryResponse] = org.http4s.circe.jsonEncoderOf[Task, QueryResponse]
  private implicit val qReqDecoder: EntityDecoder[Task, QueryRequest] = org.http4s.circe.jsonOf[Task, QueryRequest]

  def routes(implicit runtime: Runtime[AppEnv]): HttpRoutes[Task] =
    HttpRoutes.of[Task] {
      case req @ POST -> Root / "info" =>
        (for {
          s <- req.body.through(fs2.text.utf8Decode).compile.string
          conn <- SparqlConnection(parse(s).getOrElse(Json.Null))
          cache <- OntologyInfoCache()
          info <- cache.get(conn)
          resp <- Ok(info.toJson)
        } yield resp).provide(runtime.environment)

      case req @ POST -> Root / "query" =>
        (for {
          s <- req.body.through(fs2.text.utf8Decode).compile.string
          json = parse(s).getOrElse(Json.Null)
          qr <- ZIO.fromOption(json.hcursor.downField("queryRequest").focus.flatMap(_.as[QueryRequest].toOption)).mapError(_ => new IllegalArgumentException("No query request found"))
          connJson <- ZIO.fromOption(json.hcursor.downField("connectionInfo").focus).mapError(_ => new IllegalArgumentException("No connection info found"))
          conn <- SparqlConnection(connJson)
        } yield generateSelect(conn.interfaces)(qr)).foldM(e => BadRequest(e.getMessage), Ok(_))

      case req @ POST -> Root / "query" / "results" =>
        (for {
          s <- req.body.through(fs2.text.utf8Decode).compile.string
          json = parse(s).getOrElse(Json.Null)
          qr <- ZIO.fromOption(json.hcursor.downField("queryRequest").focus.flatMap(_.as[QueryRequest].toOption)).mapError(_ => new IllegalArgumentException("No query request found"))
          connJson <- ZIO.fromOption(json.hcursor.downField("connectionInfo").focus).mapError(_ => new IllegalArgumentException("No connection info found"))
          conn <- SparqlConnection(connJson)
          endpoint <- ZIO.fromOption(conn.interfaces.headOption).mapError(_ => new IllegalArgumentException("No interfaces in connection info"))
          query = generateSelect(conn.interfaces)(qr)
          selectResponse <- OntologyInfoService.sendRequest(endpoint)(query)
          parsed <- OntologyInfoService.parseResponse(selectResponse)
          subj = OntologyInfoService.getStringResultsColumn(parsed, "s")
          pred = OntologyInfoService.getStringResultsColumn(parsed, "p")
          _ <-
            if (subj.size == pred.size) ZIO.unit
            else ZIO.fail(new IllegalStateException(s"Got different subj (${subj.size}) and pred (${pred.size}) size"))
          obj = OntologyInfoService.getStringResultsColumn(parsed, "o")
          _ <-
            if (subj.size == obj.size) ZIO.unit
            else ZIO.fail(new IllegalStateException(s"Got different subj (${subj.size}) and obj (${obj.size}) size"))
          results = subj.zip(pred).zip(obj).collect { case ((Some(s), Some(p)), Some(o)) => QueryResponse(s, p, o) }
        } yield results).provide(runtime.environment).foldM(e => BadRequest(e.getMessage), Ok(_))



      //      case req @ POST -> Root / "enum" =>
      //        def rec(value: String, info: OntologyInfo): Set[String] = {
      //          @scala.annotation.tailrec
      //          def go(values: List[String], acc: Set[String], seen: Set[String]): Set[String] = values match {
      //            case Nil => acc
      //            case head :: rest =>
      //              if (seen contains head) go(rest, acc, seen)
      //              else {
      //                val subClasses = info.subClasses.getOrElse(head, Set.empty).map(_.name.fullName)
      //                go(rest ++ subClasses.toList, subClasses ++ acc, seen + head)
      //              }
      //          }
      //          go(value :: Nil, Set.empty, Set.empty)
      //        }
      //
      //        (for {
      //          jsonStr <- req.body.through(fs2.text.utf8Decode).compile.string
      //          json = parse(jsonStr).getOrElse(Json.Null)
      //          enumKey = json.hcursor.downField("enumKey").focus.flatMap(_.asString).get
      //          connJson = json.hcursor.downField("conn").focus.get
      //          conn <- SparqlConnection(connJson)
      //          cache <- OntologyInfoCache()
      //          info <- cache.get(conn)
      //          enumValues = info.enums.getOrElse(enumKey, Set.empty)
      //          allSubClasses = rec(enumKey, info)
      //          propsWithEnumRange = info.classProperties.collect { case (cls, props) if props.exists(p => allSubClasses.contains(p.range.fullName)) =>
      //            cls -> props.filter(p => allSubClasses.contains(p.range.fullName))
      //          }
      //          obj = Json.obj(
      //            "enumValues" -> enumValues.asJson,
      //            "allSubClasses" -> allSubClasses.asJson,
      //            "propsWithEnumRange" -> propsWithEnumRange.asJson)
      //          resp <- Ok(obj)
      //        } yield resp).provide(runtime.environment)
    }

  def appRoutes(blocker: Blocker): HttpRoutes[Task] = HttpRoutes.of[Task] {
    case GET -> Root =>
      Ok {
        html(
          head(
            tags2.title("Sparql Query Builder"),
            meta(name := "viewport", content := "width=device-width, initial-scale=1"),
            link(rel := "stylesheet", href := "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css"),
            link(rel := "stylesheet", href := "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"),
            link(rel := "stylesheet", href := "assets/main.css")),
          body(
            script(src := "https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"),
            script(src := "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"),
            script(src := "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"),
            div(id := "root"),
            raw(
              scalajs.html.scripts(
                projectName = "client",
                assets = name => s"/assets/$name",
                resourceExists = name => getClass.getResource(s"/public/$name") != null).body)
          ))
      }
    case req @ GET -> Root / "assets" / file =>
      (for {
        url <- OptionT.fromOption[Task](Option(getClass.getClassLoader.getResource(s"public/$file")))
        response <- StaticFile.fromURL(url, blocker, req.some)
      } yield response) getOrElseF NotFound()
  }

}

trait Named {
  val fullName: String
  lazy val (namespace, localName) = fullName.split("#").toList match {
    case ns :: name :: _ => (ns, name)
    case name :: Nil => ("", name)
    case _ => ("", "")
  }
}

final case class Name(fullName: String) extends Named
final case class Range(fullName: String) extends Named

final case class Property(name: Name, range: Range)
object Property {
  implicit val propEncoder: Encoder[Property] = p => Json.obj(
    "propName" -> Json.fromString(p.name.fullName),
    "propRange" -> Json.fromString(p.range.fullName),
  )
}

final case class Label()

final case class Class(name: Name, parentNames: Vector[Name], label: Option[Label]) {
  def addParentName(parentName: Name): Class =
    copy(parentNames = parentNames :+ parentName)
}
object Class {
  def apply(name: Name) =
    new Class(name, Vector.empty, None)
  def apply(name: Name, label: Label) =
    new Class(name, Vector.empty, label.some)
  def apply(name: Name, parentNames: Vector[Name]) =
    new Class(name, parentNames, None)
}

sealed trait SparqlEndpoint {
  def uri: String
  def defaultGraph: Option[String]
  def postUrl: String

  val (host: String, port: Int, optionalEndpoint: Option[String]) = {
    def getPortAndEndpoint(portEndpoint: String) =
      portEndpoint.split("/").toList match {
        case port :: Nil if port.nonEmpty && port.forall(_.isDigit) =>
          (port.toInt, None)
        case port :: rest if port.nonEmpty && port.forall(_.isDigit) =>
          (port.toInt, rest.mkString("/").some)
        case Nil =>
          (80, None)
        case rest =>
          (80, rest.mkString("/").some)
      }
    uri.split(":").toList match {
      case protocol :: host :: portEndpoint :: Nil if uri.startsWith("http") =>
        val (port, endpoint) = getPortAndEndpoint(portEndpoint)
        (protocol + ":" + host, port, endpoint)
      case protocol :: host :: Nil if uri.startsWith("http") =>
        (protocol + ":" + host, 80, None)
      case host :: portEndpoint :: Nil =>
        val (port, endpoint) = getPortAndEndpoint(portEndpoint)
        (host, port, endpoint)
      case hostEndpoint :: Nil =>
        hostEndpoint.split("/").toList match {
          case host :: Nil =>
            (host, 80, None)
          case host :: rest =>
            (host, 80, rest.mkString("/").some)
          case Nil => throw new IllegalArgumentException("Wrong uri")
        }
    }
  }
}
object SparqlEndpoint {
  final case class VirtuosoEndpoint(uri: String, defaultGraph: Option[String]) extends SparqlEndpoint {
    val postUrl: String = s"$host:$port/sparql"
  }
  final case class FusekiEndpoint(uri: String, defaultGraph: Option[String]) extends SparqlEndpoint {
    val endpoint: String =
      optionalEndpoint
        .getOrElse(throw new IllegalStateException(s"Fuseki URL is missing dataset: $uri"))
    val postUrl: String = s"$host:$port/$endpoint"
  }

  def instance(`type`: String, uri: String, defaultGraph: Option[String]): SparqlEndpoint =
    `type` match {
      case "virtuoso" => VirtuosoEndpoint(uri, defaultGraph)
      case "fuseki" => FusekiEndpoint(uri, defaultGraph)
      case _ => throw new IllegalArgumentException(s"Unexpected endpoint type: ${`type`}")
    }
}

final class SparqlConnection private (val interfaces: Vector[SparqlEndpoint]) {
  lazy val key: String = interfaces
    .sortBy(_.uri)
    .map(i => i.uri + i.defaultGraph.fold("")(";" + _))
    .mkString(";")
}
object SparqlConnection {
  def apply(json: Json): ZIO[Any, Throwable, SparqlConnection] = for {
    _ <- ZIO.effect(println(json.asString))
    interfaces = json.hcursor.downField("interfaces").focus.flatMap(_.asArray).flatMap(_.traverse { interfaceJson =>
      val `type` = interfaceJson.hcursor.downField("type").focus.flatMap(_.asString)
      val uri = interfaceJson.hcursor.downField("uri").focus.flatMap(_.asString)
      val defaultGraph =
        interfaceJson.hcursor.downField("dataset").focus.flatMap(_.asString)
          .orElse(interfaceJson.hcursor.downField("graph").focus.flatMap(_.asString))
      (`type`, uri).mapN(SparqlEndpoint.instance(_, _, defaultGraph))
    })
    res <- ZIO.fromOption(interfaces.map(new SparqlConnection(_)))
      .mapError(_ => new IllegalArgumentException("At least one interface must be specified for connection"))
  } yield res
}
